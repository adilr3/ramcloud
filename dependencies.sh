#!/bin/bash
# Author: Guoze Tang
# Blog: guozet.me
# Note: Install RAMCloud depandencies on Ubuntu 14.04 or 16.04

PRPCESSOR_NUM='getconf _NPROCESSORS_ONLN '
CURRENT_DIR=`pwd`
PACKAGE_DIR=$CURRENT_DIR/packages
NEW_LIB_PATH=""
NEW_INCLUDE_PATH=""
BIN_PATH=""
PKG_PATH=''

function install_dependency {
    echo "[INFO] Installing dependency: $1"
    sudo apt-get -y install $1
}

function add_lib_path {
    NEW_LIB_PATH=$NEW_LIB_PATH:$1
    echo "[INFO] Add $1 to LIB_PATH and LD_LIB_PATH."
}

function add_include_path {
    NEW_INCLUDE_PATH=$NEW_INCLUDE_PATH:$1
    echo "[INFO] Add $1 to INCLUDE_PATH."
}

function add_bin_path {
    BIN_PATH=$BIN_PATH:$1
    echo "[INFO] Add $1 to BIN_PATH."
}

function add_pkg_path {
    PKG_PATH=$KG_PATH:$1
    echo "[INFO] Add $1 to PKG_PATH."
}

function config_ld_lib {
    sudo echo "$1" > /etc/ld.so.conf.d/$2
    sudo ldconfig
}

function config_lib {
    NEW_LD_LIB_PATH="LD_LIBRARY_PATH=\$LD_LIBRARY_PATH$1"
    NEW_SS_LIB_PATH="LIBRARY_PATH=\$LIBRARY_PATH$1"
    reg_ld_str="^\s*export\s*$NEW_LD_LIB_PATH"
    reg_ss_str="^\s*export\s*$NEW_SS_LIB_PATH"
    
    if grep -q $reg_ld_str /etc/profile
    then
        echo $1 exists in LD_LIBRARY_PATH, skip
    else
        sudo echo "export $NEW_LD_LIB_PATH" >> /etc/profile
    fi

    if grep -q $reg_ss_str /etc/profile
    then
        echo $1 exists in LIBRARY_PATH, skip
    else
        sudo echo "export $NEW_SS_LIB_PATH" >> /etc/profile
        echo "[INFO] Config the lib path:$1 in /etc/profile."
    fi
}

function config_include {
    NEW_C_PATH="C_INCLUDE_PATH=\$C_INCLUDE_PATH$1"
    NEW_CPLUS_PATH="CPLUS_INCLUDE_PATH=\$CPLUS_INCLUDE_PATH$1"
    reg_c_str="^\s*export\s*$NEW_C_PATH"
    reg_cplus_str="^\s*export\s*$NEW_CPLUS_PATH"
    
    if grep -q $reg_c_str /etc/profile
    then
        echo $1 exists in C_INCLUDE_PATH, skip
    else
        sudo echo "export $NEW_C_PATH" >> /etc/profile
    fi

    if grep -q $reg_cplus_str /etc/profile
    then
        echo $1 exists in C_INCLUDE_PATH, skip
    else
        sudo echo "export $NEW_CPLUS_PATH" >> /etc/profile
        echo "[INFO] Config the include path:$1 in /etc/profile."
    fi
}

# Add bin_path of these application in /etc/profile
function config_bin {
    NEW_BIN_PATH="PATH=\$PATH$1"
    reg_bin_str="^\s*export\s*$NEW_BIN_PATH"
    
    if grep -q $reg_bin_str /etc/profile
    then
        echo $1 exists in PATH, skip
    else
        sudo echo "export $NEW_BIN_PATH" >> /etc/profile
        echo "[INFO] Config the bin path:$1 in /etc/profile."
    fi
}

# Add pkg_path of these application in /etc/profile
function config_pkg {
    NEW_PKG_PATH="PKG_CONFIG_PATH=$1"
    reg_pkg_str="^\s*export\s*$NEW_PKG_PATH"
    
    if grep -q $reg_pkg_str /etc/profile
    then
        echo $1 exists in PATH, skip
    else
        sudo echo "export $NEW_PKG_PATH" >> /etc/profile
        echo "[INFO] Config the pkg path:$1 in /etc/profile."
    fi
}

lnif() {
    if [ -e "$1" ]; then
        sudo ln -s "$1" "$2" -f
        echo "[INFO] Link $1 to $2"
    fi
}

install_dependency build-essential
install_dependency libssl-dev

# install_dependency libpcre3-dev
# install_dependency libpcre++-dev
# install_dependency git-core
# install_dependency doxygen
# install_dependency protobuf-compiler
# install_dependency libprotobuf-dev
# install_dependency libcrypto++-dev
# install_dependency libevent-dev
# install_dependency libboost-all-dev
# install_dependency libgtest-dev
# install_dependency libzookeeper-mt-dev
# install_dependency zookeeper

INSTALL_GCC=true
INSTALL_JAVA=true
INSTALL_CMAKE=true
INSTALL_PCRE=true
INSTALL_PYTHON=true
INSTALL_BOOST=true
INSTALL_DOXYGEN=true
INSTALL_PRO_BUF=true
INSTALL_ZOOKEEPER=true

if $INSTALL_GCC; then
    echo "[INFO] Begin to install GCC & G++ 4.9...."
    sudo apt-get install -y software-properties-common
    sudo add-apt-repositocry -y ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    mv /usr/bin/gcc /usr/bin/gcc.bak
    mv /usr/bin/g++ /usr/bin/g++.bak
    sudo apt-get install -y gcc-4.9
    sudo apt-get install -y g++-4.9
    echo "[OK] Complete to install GCC & G++"
    lnif /usr/bin/g++-4.9 /usr/bin/g++
    lnif /usr/bin/gcc-4.9 /usr/bin/gcc
    g++ -v
    gcc -v
    [ $? -eq 0 ] && echo "[OK] Complete installing GCC and G++" || (echo "[EROR] Fail to install GCC and G++" && exit 1)
    echo "============================="
fi

if $INSTALL_JAVA; then
    echo "Begin to install Java...."
    sudo apt-add-repository -y ppa:webupd8team/java
    sudo apt-get update
    install_dependency oracle-java8-installer
    java -version
    [ $? -eq 0 ] && echo "[OK] Complete installing Java" || (echo "[EROR] Fail to install Java" && exit 1)
    echo "============================="
fi

if $INSTALL_CMAKE; then
    echo "[INFO] Begin to install cmake...."
    sudo apt-get install -y software-properties-common
    sudo add-apt-repository -y ppa:george-edison55/cmake-3.x
    sudo apt-get update
    sudo apt-get install -y cmake
    echo "[OK] Complete to install CMAKE."
    cmake --version
    [ $? -eq 0 ] && echo "[OK] Complete installing Cmake" || (echo "[EROR] Fail to install Cmake" && exit 1)
    INSTALL_CMAKE=false
    echo "============================="
fi

if $INSTALL_PCRE; then
    echo "[INFO] Begin to install pcre...."
    cd $CURRENT_DIR/packages
    tar -xzvf pcre-8.42.tar.gz
    cd pcre-8.42
    ./configure --prefix=/usr/local/pcre
    make
    echo "[OK] Pcre build passed!"
    sudo make install
    add_include_path /usr/local/pcre/include/
    add_lib_path /usr/local/pcre/lib/
    add_bin_path /usr/local/pcre/bin/
    config_ld_lib /usr/local/pcre/lib/ pcre.conf
    echo "============================="
fi

if $INSTALL_PYTHON; then
    echo "[INFO] Begin to install Python...."
    cd $CURRENT_DIR/packages
    tar zxvf Python-2.6.6.tgz
    cd Python-2.6.6  
    ./configure --prefix=/usr/local/python2.6  
    make
    echo "[OK] Python2.6 build passed!"
    sudo make install
    echo "[OK] Python2.6 complete installing."
    lnif /usr/local/python2.6/bin/python2.6 /usr/bin/python2.6
    add_lib_path /usr/local/python2.6/lib/
    add_include_path /usr/local/python2.6/include/
    add_bin_path /usr/local/python2.6/bin/
    config_ld_lib /usr/local/python2.6/lib/ python2.6.conf
    python2.6 --version
    [ $? -eq 0 ] && echo "[OK] Complete installing Python2.6" || (echo "[EROR] Fail to install python2.6" && exit 1)
    echo "============================="
fi

if $INSTALL_BOOST; then
    echo "[INFO] Begin to install BOOST...."
    cd $CURRENT_DIR/packages
    tar -xvf boost_1_52_0.tar
    cd boost_1_52_0  
    sudo ./bootstrap.sh 
    echo "[OK] BOOST build passed!" 
    sudo ./b2 -j
    [ $? -eq 0 ] && echo "[OK] Boost build passed!" || (echo "[EROR] Boost build failed." && exit 1)
    sudo ./b2 install --prefix=/usr/local/boost_1_52_0
    [ $? -eq 0 ] && echo "[OK] Boost complete!" || (echo "[EROR] Boost failed." && exit 1)
    add_lib_path /usr/local/boost_1_52_0/lib/
    add_include_path /usr/local/boost_1_52_0/include/
    config_ld_lib /usr/local/boost_1_52_0/lib/ boost_1_52_0.conf
    echo "============================="
fi

if $INSTALL_DOXYGEN; then
    if $INSTALL_CMAKE; then
        echo "[INFO] Begin to install Cmake...."
        cd $CURRENT_DIR/packages/
        tar -xzvf cmake-3.10.3.tar.gz
        cd cmake-3.10.3  
        ./bootstrap  
        make clean
        make -j $PRPCESSOR_NUM
        echo "[OK] cmake build passed."
        sudo make install
    fi
    echo "[INFO] Begin to install Doxygen...."
    cd $CURRENT_DIR/packages
    tar -xzvf doxygen.tar.gz
    cd doxygen
    cd build
    cmake -G "Unix Makefiles" ..
    make -j $PRPCESSOR_NUM
    sudo make Install
    echo "============================="
fi

if $INSTALL_PRO_BUF; then
    echo "[INFO] Begin to install libprotobuf...."
    cd $CURRENT_DIR/packages
    tar -xzvf protobuf-2.6.1.tar.gz
    cd $CURRENT_DIR/packages/protobuf-2.6.1  
    ./configure --prefix=/usr/local/protobuf  
    make -j $PRPCESSOR_NUM
    echo "[OK] Protobuf build passed!"
    make check
    sudo make install
    add_include_path /usr/local/protobuf/include/
    add_lib_path /usr/local/protobuf/lib/
    add_bin_path /usr/local/protobuf/bin/
    add_pkg_path /usr/local/protobuf/lib/pkgconfig/
    config_ld_lib /usr/local/protobuf/lib/ protobuf.conf
    lnif /usr/local/protobuf/bin/protoc /usr/bin/protoc
    [ $? -eq 0 ] && echo "[OK] Protobuf complete!" || (echo "[EROR] Protobuf incomplete." && exit 1)
    echo "============================="
fi

if $INSTALL_ZOOKEEPER; then
    echo "[INFO] Begin to install Zookeeper...."
    cd $CURRENT_DIR/packages
    tar -xzvf zookeeper-3.3.6.tar.gz
    cd zookeeper-3.3.6/src/c  
    ./configure --prefix=/usr/local/zookeeper  
    make -j $PRPCESSOR_NUM
    echo "[OK] Zookeeper build passed."
    sudo make install
    echo "[OK] Zookeeper install passed."
    add_include_path /usr/local/zookeeper/include/c-client-src/
    add_lib_path /usr/local/zookeeper/lib/
    add_bin_path /usr/local/zookeeper/bin/
    config_ld_lib /usr/local/zookeeper/lib/ zookeeper.conf
    echo "============================="
fi

config_lib $NEW_LIB_PATH
config_include $NEW_INCLUDE_PATH
config_bin $BIN_PATH
config_pkg $PKG_PATH